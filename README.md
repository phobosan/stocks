# Stocks
CSV parser and analytic tool.
The default behavior is to execute passing only the `-f=filepath`, the in-memory maps will be used and show a full print of the output.
You can modify the behavior by add `-i`, the console will become interactive and wait for input (command list as show below). Also can tell the app to persist the file content on database, using `-p`, this way the `-f=filepath` only becomes necessary at the first time `-p` is used or when a new file comes.

#### Dependencies
- java 8+ 
- maven 3.x

#### Project dependencies
- hibernate 5.x
- jpa 2.1
- h2 1.4
- commons-csv 1.6
- gson 2.8.5
* Test
    - assertj 3.6
    - junit 5

#### Prepare project instructions
`mvn clean package` 
Execute the command on project folder and a jar file will be created.

#### Execution instructions
basic command `java -jar stock-market-0.0.1.jar` or `java -cp "target/libs/*:target/classes/" stock.StockMain` (run directly from compiled sources)

But there are a few params:
* `-i` -> starts the interactive console, that will ask for one of the follow commands:
    - max_close
    - min_close
    - high_return
    - low_return
    - avg_volume
    - exit
* `-p` -> change to the persistent in file database
* `-f=filepath` -> full path to csv file to be read, must always come last.

#### How to run the test suite
Tests implemented with **JUnit**, to run separately
`mvn test`

