package stock;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;

import stock.business.intf.StockBusinessIntf;
import stock.model.vo.StockValue;
import stock.model.vo.VolumeCalc;

public class StockBusinessMemoryTest {
	private static StockBusinessIntf business;
	private Gson gson = new Gson();

	@BeforeAll
	public static void setup() {
		StockMain stockMain;
		try {
			stockMain = new StockMain( StockBusinessPersistentTest.class.getResource( "../acoes.csv" ).getPath(), false );
			business = stockMain.getStockBusiness();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}

	@Test
	void testDayWithMaximumCloseByShare() throws IOException {
		String dayWithMaximumCloseByShare = business.getDayWithMaximumCloseByShare();
		StockValue[] fromJson = gson.fromJson( dayWithMaximumCloseByShare, StockValue[].class );
		
		assertEquals( fromJson.length, 3 );
		assertThat(fromJson).extracting("name", "date", "value").contains( 
				tuple("OGXP3", LocalDate.of( 2010, 10, 15 ), new BigDecimal( "23.27" ) ),
				tuple("PETR4", LocalDate.of( 2010, 1, 6 ), new BigDecimal( "37.50" ) ) ,
				tuple("VALE5", LocalDate.of( 2011, 1, 18 ), new BigDecimal( "53.41" ) ) );
	}

	@Test
	void testDayWithMinimumCloseByShare() throws IOException {
		String dayWithMinimumCloseByShare = business.getDayWithMinimumCloseByShare();
		StockValue[] fromJson = gson.fromJson( dayWithMinimumCloseByShare, StockValue[].class );
		assertEquals( fromJson.length, 3 );
		assertThat(fromJson).extracting("name", "date", "value").contains( 
				tuple("OGXP3", LocalDate.of( 2013, 2, 19 ), new BigDecimal( "3.15" ) ),
				tuple("PETR4", LocalDate.of( 2013, 2, 7 ), new BigDecimal( "17.50" ) ) ,
				tuple("VALE5", LocalDate.of( 2012, 9, 4 ), new BigDecimal( "32.12" ) ) );
	}

	@Test
	void testDayWithHigherReturnCloseByShare() throws IOException {
		String dayWithHigherCloseReturnByShare = business.getDayWithHigherCloseReturnByShare();
		StockValue[] fromJson = gson.fromJson( dayWithHigherCloseReturnByShare, StockValue[].class );
		assertEquals( fromJson.length, 3 );
		assertThat(fromJson).extracting("name", "date", "value").contains( 
				tuple("OGXP3", LocalDate.of( 2012, 7, 2 ), new BigDecimal( "0.1455" ) ),
				tuple("PETR4", LocalDate.of( 2011, 10, 28 ), new BigDecimal( "0.1199" ) ) ,
				tuple("VALE5", LocalDate.of( 2011, 10, 28 ), new BigDecimal( "0.1006" ) ) );
	}

	@Test
	void testDayWithLowerReturnCloseByShare() throws IOException {
		String dayWithLowerCloseReturnByShare = business.getDayWithLowerCloseReturnByShare();
		StockValue[] fromJson = gson.fromJson( dayWithLowerCloseReturnByShare, StockValue[].class );
		assertEquals( fromJson.length, 3 );
		assertThat(fromJson).extracting("name", "date", "value").contains( 
				tuple("OGXP3", LocalDate.of( 2012, 6, 27 ), new BigDecimal( "-0.2533" ) ),
				tuple("PETR4", LocalDate.of( 2011, 8, 17 ), new BigDecimal( "-0.1119" ) ) ,
				tuple("VALE5", LocalDate.of( 2011, 8, 8 ), new BigDecimal( "-0.0917" ) ) );
	}

	@Test
	void testAverageVolumeByShare() throws IOException {
		String averageVolumeByShare = business.getAverageVolumeByShare();
		VolumeCalc[] fromJson = gson.fromJson( averageVolumeByShare, VolumeCalc[].class );
		assertThat(fromJson).extracting("name", "records", "sum").contains( 
				tuple("OGXP3", new Integer( 758 ), new BigDecimal( "23407658.05" ) ),
				tuple("PETR4", new Integer( 732 ), new BigDecimal( "22285171.72" ) ) ,
				tuple("VALE5", new Integer( 750 ), new BigDecimal( "16395768.00" ) ) );
	}
}
