package stock;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import stock.business.StockBusinessMemory;
import stock.business.StockBusinessPersistent;

public class StockMainTest {
	
	@Test
	void testMemoryDefaultPrint() throws IOException {
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		String output = Files.lines(Paths.get(StockBusinessPersistentTest.class.getResource( "../output.txt" ).getPath()))
				.reduce((a , b) -> a +"\n"+ b ).get().trim();
		StockMain.main( new String[] { "-f="+StockBusinessPersistentTest.class.getResource( "../acoes.csv" ).getPath() } );
		assertThat( outContent.toString() ).contains( output );
	}
	
	@Test
	void testPersistentParameter() throws IOException {
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		String output = Files.lines(Paths.get(StockBusinessPersistentTest.class.getResource( "../output.txt" ).getPath()))
				.reduce((a , b) -> a +"\n"+ b ).get().trim();
		StockMain.main( new String[] { "-p", "-f="+StockBusinessPersistentTest.class.getResource( "../acoes.csv" ).getPath() } );
		assertThat( outContent.toString() ).contains( output );
	}
	
	@Test
	void testInteractiveMemory() throws IOException {
		InputStream stdin = System.in;
		ByteArrayOutputStream outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
		String output = Files.lines(Paths.get(StockBusinessPersistentTest.class.getResource( "../output_max_close.txt" ).getPath()))
				.reduce((a , b) -> a +"\n"+ b ).get().trim();
		ByteArrayInputStream in = new ByteArrayInputStream("max_close\r\nexit".getBytes());
		System.setIn(in);
		StockMain.main( new String[] { "-i", "-f="+StockBusinessPersistentTest.class.getResource( "../acoes.csv" ).getPath() } );
		System.setIn(stdin);
		assertThat( outContent.toString() ).contains( "new command" );
		assertThat( outContent.toString() ).contains( output );
	}
}
