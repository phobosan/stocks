package stock.util;

import stock.model.Stock;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Comparator;

public class GenericComparator implements Comparator<Stock> {

	private String field;

	public GenericComparator( String field ) {
		this.field = field;
	}

	@Override
	public int compare( Stock o1, Stock o2 ) {
		Method methodO1 = null;
		Method methodO2 = null;
		try {
			methodO1 = o1.getClass().getMethod( "get" + field );
			methodO2 = o2.getClass().getMethod( "get" + field );
			return ( (BigDecimal) methodO1.invoke( o1 ) ).compareTo( (BigDecimal) methodO2.invoke( o2 ) );
		} catch ( NoSuchMethodException | SecurityException
				| IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e ) {
			e.printStackTrace();
		}
		return 0;
	}

}
