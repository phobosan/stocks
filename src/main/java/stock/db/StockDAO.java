package stock.db;

import java.util.List;

import javax.persistence.EntityTransaction;

import stock.model.Stock;

public class StockDAO extends DAO {

	public void save( Stock stock ) {
		getEm().getTransaction().begin();
		getEm().persist( stock );
		getEm().getTransaction().commit();
	}

	public void save( List<Stock> stocks ) {
		EntityTransaction txn = getEm().getTransaction();
	    txn.begin();
		stocks.stream().forEach( stock -> {
			int batchCount = 0;
			if ( batchCount > 0 && batchCount % batchSize == 0 ) {
				getEm().flush();
				getEm().clear();
			}

			getEm().persist( stock );
			batchCount++;
		} );
		txn.commit();
	}
}
