package stock.db;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class DAO {

	private EntityManager em;
	protected int batchSize = 200;
	
	public DAO() {
		Logger log = Logger.getLogger("org.hibernate");
	    log.setLevel(Level.SEVERE); 
		EntityManagerFactory emf = Persistence.createEntityManagerFactory( "pu" );
		em = emf.createEntityManager();
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm( EntityManager em ) {
		this.em = em;
	}

}
