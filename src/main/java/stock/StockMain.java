package stock;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import stock.business.StockBusinessMemory;
import stock.business.StockBusinessPersistent;
import stock.business.intf.StockBusinessIntf;
import stock.model.Stock;

public class StockMain {
	private String file = "";
	private StockBusinessIntf stockBusiness;

	public static void main( String[] args ) {
		try {
			List<String> arguments = Arrays.asList( args );
			String[] possibleFilePath = arguments.get( arguments.size() - 1 ).split( "=" );
			StockMain stockMain = new StockMain( 
					possibleFilePath.length > 1 ? possibleFilePath[1] : null, 
					arguments.contains( "-p" ) );

			if ( arguments.contains( "-i" ) ) {
				stockMain.interactiveLoop();
			} else {
				stockMain.printAllInformation();
			}
		} catch ( FileNotFoundException e ) {
			System.out.println( "File not found" );
		} catch ( IllegalArgumentException e ) {
			System.out.println( "Corrupted CSV file" );
		} catch ( IOException e ) {
			System.out.println( "Unable to read file" );
		} 
	}
	
	public StockMain() {}
	
	public StockMain( String file, boolean persistent ) throws IOException {
		this.file = file;
		if ( persistent )
			stockBusiness = new StockBusinessPersistent();
		else
			stockBusiness = new StockBusinessMemory();
		if ( file != null )
			parseCSVFile();
		stockBusiness.consolidate();
	}
	
	private void interactiveLoop() {
		Scanner scanner = new Scanner( System.in );
		System.out.println( "new command: " );
		boolean keepOn = true;
		while ( keepOn ) {
			switch ( scanner.nextLine() ) {
				case "max_close":
					printDayWithMaximumCloseByShare();
					break;
				case "min_close":
					printDayWithMinimumCloseByShare();
					break;
				case "high_return":
					printDayWithHigherCloseReturnByShare();
					break;
				case "low_return":
					printDayWithLowerCloseReturnByShare();
					break;
				case "avg_volume":
					printAverageVolumeByShare();
					break;
				case "exit":
					keepOn = false;
					break;
			}
		}
		scanner.close();
	}
	
	public void printAllInformation() throws IOException {
		printDayWithMaximumCloseByShare();
		printDayWithMinimumCloseByShare();
		printDayWithHigherCloseReturnByShare();
		printDayWithLowerCloseReturnByShare();
		printAverageVolumeByShare();
	}

	private void parseCSVFile() throws IOException {
		Reader in = new FileReader( file );
		Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse( in );
		DateTimeFormatter pattern = DateTimeFormatter.ofPattern( "yyyy-MM-dd" );
		for ( CSVRecord record : records ) {
			Stock stock = new Stock( 
					record.get( "Acao" ), 
					LocalDate.parse( record.get( "Data" ), pattern ), 
					record.get( "Close" ), 
					record.get( "Volume" ) );
			stockBusiness.add( stock );
		}
		in.close();
	}

	public StockBusinessIntf getStockBusiness() {
		return stockBusiness;
	}

	private void printDayWithMaximumCloseByShare() {
		System.out.println( "**Maximum Close** day by Share**" );
		System.out.println( stockBusiness.getDayWithMaximumCloseByShare() );
		System.out.println( "*********************" );
	}

	private void printDayWithMinimumCloseByShare() {
		System.out.println( "**Minimum Close** day by Share**" );
		System.out.println( stockBusiness.getDayWithMinimumCloseByShare() );
		System.out.println( "*********************" );
	}

	private void printDayWithHigherCloseReturnByShare() {
		System.out.println( "**Higher Return** day by Share**" );
		System.out.println( stockBusiness.getDayWithHigherCloseReturnByShare() );
		System.out.println( "*********************" );
	}

	private void printDayWithLowerCloseReturnByShare() {
		System.out.println( "**Lower Return** day by Share**" );
		System.out.println( stockBusiness.getDayWithLowerCloseReturnByShare() );
		System.out.println( "*********************" );
	}

	private void printAverageVolumeByShare() {
		System.out.println( "**Average Volume** by Share**" );
		System.out.println( stockBusiness.getAverageVolumeByShare() );
		System.out.println( "*********************" );
	}
}
