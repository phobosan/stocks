package stock.model.vo;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.google.gson.annotations.SerializedName;

import stock.model.Stock;

public class StockValue {

	private String name;

	private LocalDate date;
	
	@SerializedName(value="value", alternate={"close", "closeReturn"})
	private BigDecimal value;

	public StockValue( String name, LocalDate date, BigDecimal value ) {
		this.name = name;
		this.date = date;
		this.value = value;
	}
	
	public StockValue( Stock stock, BigDecimal value ) {
		this.name = stock.getName();
		this.date = stock.getDate();
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate( LocalDate date ) {
		this.date = date;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue( BigDecimal value ) {
		this.value = value;
	}

}
