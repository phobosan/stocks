package stock.model.vo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class VolumeCalc {
	private String name;
	private Integer records;
	private BigDecimal sum;

	public VolumeCalc() {}

	public VolumeCalc( String name, Integer records, BigDecimal sum ) {
		this.name = name;
		this.records = records;
		this.sum = sum.setScale( 2, RoundingMode.HALF_EVEN );
	}
	
	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public Integer getRecords() {
		return records;
	}

	public void setRecords( Integer records ) {
		this.records = records;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum( BigDecimal sum ) {
		this.sum = sum;
	}

}