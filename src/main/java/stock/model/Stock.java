package stock.model;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import stock.model.vo.StockValue;
import stock.model.vo.VolumeCalc;

@Entity
@Table( uniqueConstraints = @UniqueConstraint( columnNames = { "name", "date" } ) )
@SqlResultSetMappings( { 
	@SqlResultSetMapping( 
			name = "StockValueMapping", 
			classes = @ConstructorResult( targetClass = StockValue.class, 
			columns = { @ColumnResult( name = "name" ), @ColumnResult( name = "date", type = LocalDate.class ), @ColumnResult( name = "value", type = BigDecimal.class ) } ) ), 
	@SqlResultSetMapping( 
			name = "VolumeCalcMapping", 
			classes = @ConstructorResult( targetClass = VolumeCalc.class, 
			columns = { @ColumnResult( name = "name" ), @ColumnResult( name = "records", type = Integer.class ),	@ColumnResult( name = "sum", type = BigDecimal.class ) } ) ) 
	} )
@NamedNativeQueries( { 
	@NamedNativeQuery( name = "SQL_ALL_STOCK_NAME", query = "SELECT DISTINCT s.name from Stock s ORDER BY s.name asc " ), 
	@NamedNativeQuery( name = "SQL_MIN_CLOSE_BY_SHARE", query = "SELECT s.name, s.date, s.close as value from Stock s WHERE s.name = :name ORDER BY s.close asc LIMIT 1", resultSetMapping = "StockValueMapping" ), 
	@NamedNativeQuery( name = "SQL_MAX_CLOSE_BY_SHARE", query = "SELECT s.name, s.date, s.close as value from Stock s WHERE s.name = :name ORDER BY s.close desc LIMIT 1", resultSetMapping = "StockValueMapping" ), 
	@NamedNativeQuery( name = "SQL_MIN_RETURN_CLOSE_BY_SHARE", query = "SELECT s.name, s.date, s.closereturn as value from Stock s WHERE s.name = :name AND s.closereturn is not null ORDER BY s.closereturn asc LIMIT 1", resultSetMapping = "StockValueMapping" ), 
	@NamedNativeQuery( name = "SQL_MAX_RETURN_CLOSE_BY_SHARE", query = "SELECT s.name, s.date, s.closereturn as value from Stock s WHERE s.name = :name AND s.closereturn is not null ORDER BY s.closereturn desc LIMIT 1", resultSetMapping = "StockValueMapping" ), 
	@NamedNativeQuery( name = "SQL_AVG_VOLUME_BY_SHARE", query = "SELECT s.name as name, COUNT(s.id) as records, AVG(s.volume) as sum FROM Stock s WHERE s.volume > 0 GROUP BY s.name", resultSetMapping = "VolumeCalcMapping" ) 
	} ) 
public class Stock {
	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE )
	private Long id;

	private String name;

	private LocalDate date;

	private BigDecimal close;

	private BigDecimal volume;

	@Column( precision = 19, scale = 4 )
	private BigDecimal closeReturn;

	public Stock() {}

	public Stock( String name, LocalDate date, String close, String volume ) {
		this.name = name;
		this.date = date;
		this.close = new BigDecimal( close );
		this.volume = new BigDecimal( volume );
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate( LocalDate date ) {
		this.date = date;
	}

	public BigDecimal getClose() {
		return close;
	}

	public void setClose( BigDecimal close ) {
		this.close = close;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume( BigDecimal volume ) {
		this.volume = volume;
	}

	public BigDecimal getCloseReturn() {
		return closeReturn;
	}

	public void setCloseReturn( BigDecimal closeReturn ) {
		this.closeReturn = closeReturn;
	}

	public void calculateCloseReturn( Stock previous ) {
		closeReturn = previous == null ? null : getClose().divide( previous.getClose(), MathContext.DECIMAL128 )
				.subtract( BigDecimal.ONE ).setScale( 4, RoundingMode.HALF_EVEN );
	}

	@Override
	public String toString() {
		return "Stock [name=" + name + ", date=" + date + ", close=" + close + ", volume=" + volume + ", closeReturn=" + closeReturn + "]";
	}

}