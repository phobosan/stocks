package stock.business.intf;

import stock.model.Stock;

public interface StockBusinessIntf {

	public Stock add( Stock stock );

	public String getDayWithMaximumCloseByShare();

	public String getDayWithMinimumCloseByShare();

	public String getDayWithHigherCloseReturnByShare();

	public String getDayWithLowerCloseReturnByShare();

	public String getAverageVolumeByShare();
	
	public void consolidate();
}
