package stock.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.Query;

import com.google.gson.Gson;

import stock.business.intf.StockBusinessIntf;
import stock.db.StockDAO;
import stock.model.Stock;
import stock.model.vo.VolumeCalc;

public class StockBusinessPersistent implements StockBusinessIntf {
	private StockDAO stockDAO;
	private Gson gson = new Gson();
	private Map<String, Stock> mapLastStocksByShare = new TreeMap<>();
	private List<String> shares;
	private List<Stock> toPersist = new ArrayList<>();

	public StockBusinessPersistent() {
		stockDAO = new StockDAO();
	}

	@Override
	public Stock add( Stock stock ) {
		stock.calculateCloseReturn( mapLastStocksByShare.get( stock.getName() ) );
		mapLastStocksByShare.put( stock.getName(), stock );
		toPersist.add( stock );
		return stock;
	}

	public String getDayWithMaximumCloseByShare() {
		return printInformation( "SQL_MAX_CLOSE_BY_SHARE" );
	}

	public String getDayWithMinimumCloseByShare() {
		return printInformation( "SQL_MIN_CLOSE_BY_SHARE" );
	}

	public String getDayWithHigherCloseReturnByShare() {
		return printInformation( "SQL_MAX_RETURN_CLOSE_BY_SHARE" );
	}

	public String getDayWithLowerCloseReturnByShare() {
		return printInformation( "SQL_MIN_RETURN_CLOSE_BY_SHARE" );
	}

	public String getAverageVolumeByShare() {
		Query query = stockDAO.getEm().createNamedQuery( "SQL_AVG_VOLUME_BY_SHARE" );
		return gson.toJson( query.getResultList() );
	}

	@SuppressWarnings( "unchecked" )
	public void consolidate() {
		stockDAO.save( toPersist );
		Query query = stockDAO.getEm().createNamedQuery( "SQL_ALL_STOCK_NAME" );
		shares = query.getResultList();
	}

	@SuppressWarnings( "unchecked" )
	private String printInformation( String namedQuery ) {
		List<Object[]> bag = new ArrayList<>();
		shares.stream().forEach( name -> {
			Query query = stockDAO.getEm().createNamedQuery( namedQuery );
			query.setParameter( "name", name );
			bag.addAll( query.getResultList() );
		} );
		return gson.toJson( bag );
	}

}
