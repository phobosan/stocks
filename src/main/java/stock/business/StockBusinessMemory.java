package stock.business;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.google.gson.Gson;

import stock.business.intf.StockBusinessIntf;
import stock.model.Stock;
import stock.model.vo.StockValue;
import stock.model.vo.VolumeCalc;
import stock.util.GenericComparator;

public class StockBusinessMemory implements StockBusinessIntf {
	private Map<String, List<Stock>> data = new TreeMap<>();
	private Map<String, List<Stock>> orderedByClose = new TreeMap<>();
	private Map<String, List<Stock>> orderedByReturnClose = new TreeMap<>();
	private Map<String, Stock> mapLastStocksByShare = new TreeMap<>();
	private Gson gson = new Gson();

	public StockBusinessMemory() {}

	public Stock add( Stock stock ) {
		stock.calculateCloseReturn( mapLastStocksByShare.get( stock.getName() ) );
		if ( data.containsKey( stock.getName() ) ) {
			data.get( stock.getName() ).add( stock );
		} else {
			List<Stock> list = new ArrayList<>();
			list.add( stock );
			data.put( stock.getName(), list );
		}
		mapLastStocksByShare.put( stock.getName(), stock );
		return stock;
	}

	public String getDayWithMaximumCloseByShare() {
		return printInformation( orderedByClose, "last", true );
	}

	public String getDayWithMinimumCloseByShare() {
		return printInformation( orderedByClose, "first", true );
	}

	public String getDayWithHigherCloseReturnByShare() {
		return printInformation( orderedByReturnClose, "last", false );
	}

	public String getDayWithLowerCloseReturnByShare() {
		return printInformation( orderedByReturnClose, "first", false );
	}

	public String getAverageVolumeByShare() {
		List<VolumeCalc> bag = new ArrayList<>();
		data.keySet().stream().forEachOrdered( name -> {
			Collection<Stock> values = orderedByClose.get( name );
			BigDecimal[] sum = values.stream()
					.filter( stock -> stock.getVolume().compareTo( BigDecimal.ZERO ) > 0 )
					.map( stock -> new BigDecimal[] { stock.getVolume(), BigDecimal.ONE } )
					.reduce( ( a, b ) -> new BigDecimal[] { a[0].add( b[0] ), a[1].add( BigDecimal.ONE ) } )
					.get();
			bag.add( new VolumeCalc( name, sum[1].intValue(), sum[0].divide( sum[1], MathContext.DECIMAL128 ).setScale( 2, RoundingMode.HALF_EVEN ) ) );
		} );
		return gson.toJson( bag );
	}

	public void consolidate() {
		data.keySet().stream().forEachOrdered( name -> {
			List<Stock> values = data.get( name ).stream()
					.sorted( new GenericComparator( "Close" ) )
					.collect( Collectors.toList() );
			orderedByClose.put( name, values );
			
			values = data.get( name ).stream()
					.filter( stock -> stock.getCloseReturn() != null )
					.sorted( new GenericComparator( "CloseReturn" ) )
					.collect( Collectors.toList() );
			orderedByReturnClose.put( name, values );
		} );
	}

	private String printInformation( Map<String, List<Stock>> map, String position, boolean closeField ) {
		List<StockValue> bag = new ArrayList<>();
		map.keySet().stream().forEach( name -> {
			List<Stock> values = map.get( name );
			Integer index = null;
			if ( position.equals( "last" ) ) {
				index = values.size() - 1;
			} else if ( position.equals( "first" ) ) {
				index = 0;
			}
			Stock stock = values.get( index );
			bag.add( new StockValue( stock, closeField ? stock.getClose() : stock.getCloseReturn() ) );
		} );
		return gson.toJson( bag );
	}

}
